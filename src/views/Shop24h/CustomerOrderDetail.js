import React, { useEffect, useState } from 'react'
import { CCard, CCardBody, CCardHeader } from '@coreui/react'
import {
  Box,
  Button,
  Divider,
  FormControl,
  Grid,
  IconButton,
  InputBase,
  MenuItem,
  Modal,
  Pagination,
  Paper,
  Select,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextareaAutosize,
  TextField,
  Typography,
} from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete'
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart'
import PostAddIcon from '@mui/icons-material/PostAdd'
import UploadIcon from '@mui/icons-material/Upload'
import ManageSearchIcon from '@mui/icons-material/ManageSearch'
import { useParams } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { useSnackbar } from 'notistack'
import RowCollapse from 'src/components/RowCollapse'

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 2,
}

const CustomerOrderDetail = () => {
  const { enqueueSnackbar } = useSnackbar()
  //lấy userId từ param
  const { CustomerId } = useParams()
  //khai báo biến
  const [customerName, setCustomerName] = useState('')
  const [ordersData, setOrdersData] = useState([
    {
      cost: 0,
      note: '',
      orderDate: '',
      shippedDate: '',
      orderDetail: [
        {
          productId: '',
          productImg: 'logo.jpg',
          productName: '',
          productPrice: 0,
          productQuantity: 0,
        },
      ],
    },
  ])
  const [productsData, setProductsData] = useState([])
  //biến redux
  const dispatch = useDispatch()
  const productCartsArr = useSelector((state) => state.productCartsArr)
  const total = useSelector((state) => state.total)
  //biến giỏ hàng
  const [productEleAddCart, setProductEleAddCart] = useState({})
  const [productIdAddCart, setProductIdAddCart] = useState('0')
  const [productNameAddCart, setProductNameAddCart] = useState('')
  const [productImageAddCart, setProductImageAddCart] = useState('logo.jpg')
  const [productPriceAddCart, setProductPriceAddCart] = useState(0)
  const [productQuantityAddCart, setProductQuantityAddCart] = useState(1)
  //các input trong modal
  const [noteModal, setNoteModal] = useState('')
  const [orderDateModal, setOrderDateModal] = useState('')
  const [shippedDateModal, setShippedDateModal] = useState('')
  const [orderIdModal, setOrderIdModal] = useState('0')
  //biến dùng để mount lại page
  const [refresh, setRefresh] = useState(false)
  const getRefresh = () => {
    refresh === true ? setRefresh(false) : setRefresh(true)
  }
  //set page pagination
  const [noPage, setNoPage] = useState(0)
  const [page, setPage] = useState(1)
  const onPageChange = (event, value) => {
    setPage(value)
  }
  //Search
  const [inputOrderIdSearch, setInputOrderIdSearch] = useState('')
  const [condition, setCondition] = useState('')
  const onBtnSearchClickHandle = () => {
    if (inputOrderIdSearch.match(/^[0-9a-fA-F]{24}$/) || inputOrderIdSearch === '') {
      setCondition(inputOrderIdSearch)
    } else {
      enqueueSnackbar('ID không chính xác', { variant: 'error' })
    }
  }
  //khai báo url
  const URL = 'http://localhost:8000/Customers/' + CustomerId + '/Orders?orderid=' + condition

  //khai báo fetch
  const fetchApi = async (urlApi, bodyApi) => {
    const response = await fetch(urlApi, bodyApi)
    const data = await response.json()
    return data
  }

  //<-------------------------------------------------------------------------------------------------Giỏ hàng------------
  //sự kiện chọn sản phẩm trong select
  const onSelectProductAddCartChange = (value) => {
    setProductEleAddCart(value)
    setProductImageAddCart(value.imageUrl)
    setProductPriceAddCart(value.promotionPrice)
    setProductIdAddCart(value._id)
    setProductNameAddCart(value.name)
  }
  //sự kiện thay đổi giá trị quantity phần chọn sản phẩm
  const onInpQuantityAddCartChange = (value) => {
    if (value > 0) {
      setProductQuantityAddCart(parseInt(value))
    } else {
      enqueueSnackbar('số lượng phải > 0', { variant: 'error' })
    }
  }
  //sự kiện click add sản phẩm vào giỏ hàng
  const onBtnAddProductToCartClick = () => {
    let productAdd = {
      productId: productIdAddCart,
      productName: productNameAddCart,
      productImg: productImageAddCart,
      productPrice: productPriceAddCart,
      productQuantity: productQuantityAddCart,
    }
    if (productAdd.productId === '0') {
      enqueueSnackbar('Hãy chọn sản phẩm!!!', { variant: 'error' })
    } else {
      dispatch({
        type: 'ADD_CART',
        payload: {
          product: productAdd,
        },
      })
    }
  }
  //xử lý sự kiện click button delete trong giỏ hàng
  const onClickDeleteProductCartHandle = (value) => {
    dispatch({
      type: 'DELETE_ITEM_CART',
      payload: {
        product: value,
      },
    })
  }
  //xử lý sự kiện thay đổi số lượng trong giỏ hảng
  const onProductCartQuantityChange = (quantittyChange, productElement) => {
    if (quantittyChange > 0) {
      dispatch({
        type: 'UPDATE_ITEM_CART',
        payload: {
          product: {
            productId: productElement.productId,
            productName: productElement.productName,
            productImg: productElement.productImg,
            productPrice: productElement.productPrice,
            productQuantity: parseInt(quantittyChange),
            totalAdd: productElement.productPrice * parseInt(quantittyChange),
          },
        },
      })
    } else {
      enqueueSnackbar(
        'Số lượng phải là số và lớn hơn 0. Nếu bạn không muốn sản phẩm này, hãy bấm nút xóa!',
        { variant: 'error' },
      )
    }
  }
  //reset giỏ hàng
  const resetCart = () => {
    dispatch({
      type: 'RESET_CART',
      payload: {},
    })
  }
  //-----------------------------------------------------------------------------------------------------end Giỏ hàng------>

  //<---------------------------------------------------------------------------------------- Modal Create--------
  //Mở, đóng modal create Order
  const [openModalCreateOrder, setOpenModalCreateOrder] = useState(false)
  const handleOpenModalCreateOrder = () => setOpenModalCreateOrder(true)
  const handleCloseModalCreateOrder = () => {
    setOpenModalCreateOrder(false)
    resetCart()
    resetModal()
  }
  //Hàm xử lý sự kiện khi click button tạo đơn
  const onSubmitCreateModalHandle = (event) => {
    event.preventDefault()
    //1. thu thập dữ liệu
    let orderData = {
      orderDetail: productCartsArr,
      cost: total,
      note: noteModal,
    }
    //2.validate
    if (productCartsArr.length > 0) {
      let body = {
        method: 'POST',
        body: JSON.stringify(orderData),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      }
      //3.call api
      fetchApi('http://localhost:8000/Customers/' + CustomerId + '/Orders', body)
        .then((data) => {
          enqueueSnackbar('Đã đặt hàng thành công. OrderId: ' + data.data._id, {
            variant: 'success',
          })
          //đóng modal, reset dữ liệu
          handleCloseModalCreateOrder()
          getRefresh()
        })
        .catch((err) => {
          enqueueSnackbar('Lỗi!!! Chưa tạo được order!', { variant: 'error' })
        })
    } else {
      enqueueSnackbar('Chưa có sản phẩm trong giỏ hàng, hãy chọn sản phẩm!', { variant: 'error' })
    }
  }
  //----------------------------------------------------------------------------------------end Modal Create-------->

  //<---------------------------------------------------------------------------------------- Modal Update--------
  //Mở, đóng modal update Order
  const [openModalUpdateOrder, setOpenModalUpdateOrder] = useState(false)
  const handleOpenModalUpdateOrder = () => setOpenModalUpdateOrder(true)
  const handleCloseModalUpdateOrder = () => {
    setOpenModalUpdateOrder(false)
    resetCart()
    resetModal()
  }
  //hàm xử lý sự kiện khi click btn info
  const onClickBtnInfoHandle = (orderDataRow) => {
    dispatch({
      type: 'SET_NEW_CART',
      payload: {
        product: orderDataRow.orderDetail,
        total: orderDataRow.cost,
      },
    })
    setOrderIdModal(orderDataRow._id)
    setNoteModal(orderDataRow.note)
    setOrderDateModal(orderDataRow.orderDate.slice(0, 10))
    orderDataRow.shippedDate
      ? setShippedDateModal(orderDataRow.shippedDate.slice(0, 10))
      : setShippedDateModal('')
    handleOpenModalUpdateOrder()
  }
  //Hàm xử lý sự kiện khi click button update order
  const onSubmitUpdateModalHandle = (event) => {
    event.preventDefault()
    //1.thu thập dữ liệu
    let orderData = {
      orderDetail: productCartsArr,
      cost: total,
      note: noteModal,
      shippedDate: shippedDateModal,
    }
    //2.validate
    if (productCartsArr.length > 0) {
      let body = {
        method: 'PUT',
        body: JSON.stringify(orderData),
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      }
      //3.call api
      fetchApi('http://localhost:8000/Orders/' + orderIdModal, body)
        .then((data) => {
          enqueueSnackbar('Đã cập nhật thành công!', { variant: 'success' })
          //đóng modal, reset dữ liệu
          handleCloseModalUpdateOrder()
          getRefresh()
        })
        .catch((err) => {
          enqueueSnackbar('Lỗi!!! Chưa cập nhật được order!', { variant: 'error' })
        })
    } else {
      enqueueSnackbar('Chưa có sản phẩm trong giỏ hàng, hãy chọn sản phẩm!', { variant: 'error' })
    }
  }
  //----------------------------------------------------------------------------------------end Modal Update-------->

  //reset modal
  const resetModal = () => {
    setNoteModal('')
    setOrderDateModal('')
    setShippedDateModal('')
  }

  //<------------------------------------------------------------------------------------------- Modal delete
  //Mở, đóng modal Delete order
  const [openModalDeleteOrder, setOpenModalDeleteOrder] = useState(false)
  const handleOpenModalDeleteOrder = () => setOpenModalDeleteOrder(true)
  const handleCloseModalDeleteOrder = () => setOpenModalDeleteOrder(false)
  const [orderIdDeleteModal, setOrderIdDeleteModal] = useState(0)

  //xử lý sự kiện click button delete
  const onClickBtnDeleteHandle = (orderRow) => {
    setOrderIdDeleteModal(orderRow._id)
    handleOpenModalDeleteOrder()
  }

  //Hàm xử lý sự kiện khi click confirm delete order
  const onSubmitDeleteModal = (event) => {
    event.preventDefault()
    let id = orderIdDeleteModal
    let body = {
      method: 'DELETE',
    }
    fetch('http://localhost:8000/Customers/' + CustomerId + '/Orders/' + id, body)
      .then(() => {
        enqueueSnackbar('Đã Xóa đơn hàng thành công!', { variant: 'success' })
        //đóng modal
        handleCloseModalDeleteOrder()
        //refresh table
        setPage(1)
        getRefresh()
      })
      .catch((error) => {
        throw error
      })
  }
  //end modal delete handle---------------------------------------------------------------------------------->

  useEffect(() => {
    fetchApi('http://localhost:8000/Customers/' + CustomerId)
      .then((data) => {
        setCustomerName(data.data.fullName)
      })
      .catch((err) => {
        throw err
      })
    fetchApi(URL)
      .then((data) => {
        setOrdersData(data.data)
        setNoPage(Math.ceil(data.data.length / 5))
      })
      .catch((err) => {
        throw err
      })
    fetchApi('http://localhost:8000/Products')
      .then((data) => {
        setProductsData(data.data)
      })
      .catch((err) => {
        throw err
      })
  }, [refresh, CustomerId, URL])

  return (
    <div>
      <CCard className="mb-4">
        <CCardHeader>
          <h5>All Oders of: {customerName}</h5>
        </CCardHeader>
        <CCardBody>
          <Grid container justifyContent="space-between" alignItems="center" marginBottom={2}>
            <Grid item>
              <Button
                color="success"
                variant="contained"
                startIcon={<PostAddIcon />}
                className="float-end"
                onClick={handleOpenModalCreateOrder}
              >
                Create
              </Button>
            </Grid>
            <Grid item>
              <Paper component="form" sx={{ display: 'flex', alignItems: 'center', paddingX: 2 }}>
                <InputBase
                  sx={{ width: 220 }}
                  placeholder="Search Order ID....."
                  value={inputOrderIdSearch}
                  onChange={(event) => setInputOrderIdSearch(event.target.value)}
                />
                <IconButton
                  type="submit"
                  sx={{ p: '10px' }}
                  aria-label="search"
                  onClick={onBtnSearchClickHandle}
                >
                  <ManageSearchIcon />
                </IconButton>
              </Paper>
            </Grid>
          </Grid>
          <TableContainer component={Paper}>
            <Table>
              <TableHead>
                <TableRow className="bg-secondary bg-gradient">
                  <TableCell />
                  <TableCell>Order ID</TableCell>
                  <TableCell align="center">Order Date</TableCell>
                  <TableCell align="center">Shipped Date</TableCell>
                  <TableCell align="center">Cost</TableCell>
                  <TableCell align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {ordersData.length > 0 ? (
                  ordersData.slice(5 * (page - 1), 5 * page).map((orderRow, index) => {
                    return (
                      <RowCollapse
                        key={index}
                        orderRow={orderRow}
                        onClickBtnDeleteHandle={onClickBtnDeleteHandle}
                        onClickBtnInfoHandle={onClickBtnInfoHandle}
                        statusDel="on"
                      />
                    )
                  })
                ) : (
                  <TableRow>
                    <TableCell colSpan={5}>No Order...</TableCell>
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </TableContainer>
          <Grid sx={{ display: 'flex', justifyContent: 'flex-end' }}>
            <Pagination
              showFirstButton
              showLastButton
              count={noPage}
              page={page}
              onChange={onPageChange}
            />
          </Grid>
        </CCardBody>
      </CCard>

      {/* Modal Create Order */}
      <Modal open={openModalCreateOrder}>
        <Box sx={style} style={{ width: '90%', maxHeight: '95%', overflow: 'auto' }}>
          <h4 className="text-center">Create Order</h4>
          <form onSubmit={onSubmitCreateModalHandle}>
            <TableContainer component={Paper}>
              <Table aria-label="cart table">
                <TableHead>
                  <TableRow className="bg-secondary bg-gradient">
                    <TableCell align="center" colSpan={3} sx={{ color: 'white' }}>
                      PRODUCT
                    </TableCell>
                    <TableCell align="center" sx={{ color: 'white' }}>
                      PRICE
                    </TableCell>
                    <TableCell align="center" sx={{ color: 'white' }}>
                      QUANTITY
                    </TableCell>
                    <TableCell align="center" sx={{ color: 'white' }}>
                      SUBTOTAL
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell align="left">
                      <IconButton onClick={onBtnAddProductToCartClick}>
                        <AddShoppingCartIcon fontSize="small" />
                      </IconButton>
                    </TableCell>
                    <TableCell align="right">
                      <img
                        src={require('../../assets/images/Products/' + productImageAddCart)}
                        className="img-thumbnail"
                        alt="product"
                        height="48px"
                        width="48px"
                      />
                    </TableCell>
                    <TableCell align="left">
                      <FormControl sx={{ minWidth: 350 }} size="small">
                        <Select
                          displayEmpty
                          inputProps={{ 'aria-label': 'Without label' }}
                          fullWidth
                          value={productEleAddCart}
                          onChange={(event) => onSelectProductAddCartChange(event.target.value)}
                        >
                          {productsData.length > 0 ? (
                            productsData.map((ele, index) => {
                              return (
                                <MenuItem key={index} value={ele}>
                                  {ele.name}
                                </MenuItem>
                              )
                            })
                          ) : (
                            <MenuItem></MenuItem>
                          )}
                        </Select>
                      </FormControl>
                    </TableCell>
                    <TableCell align="center">
                      {productPriceAddCart.toLocaleString('vi-VN', {
                        style: 'currency',
                        currency: 'VND',
                      })}
                    </TableCell>
                    <TableCell align="center">
                      <input
                        type="number"
                        value={productQuantityAddCart}
                        className="form-control"
                        style={{ width: '75px' }}
                        onChange={(event) => onInpQuantityAddCartChange(event.target.value)}
                      />
                    </TableCell>
                    <TableCell align="center">
                      {(productPriceAddCart * productQuantityAddCart).toLocaleString('vi-VN', {
                        style: 'currency',
                        currency: 'VND',
                      })}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            <h6 className="pt-3">Your Cart:</h6>
            <Paper style={{ maxHeight: 180, overflow: 'auto' }}>
              <TableContainer component={Paper}>
                <Table aria-label="cart table">
                  <TableBody>
                    {productCartsArr.length > 0 ? (
                      productCartsArr.map((productCartEle, index) => {
                        return (
                          <TableRow key={index}>
                            <TableCell align="left">
                              <IconButton
                                onClick={() => onClickDeleteProductCartHandle(productCartEle)}
                              >
                                <DeleteIcon fontSize="small" />
                              </IconButton>
                            </TableCell>
                            <TableCell align="right">
                              <img
                                src={require('../../assets/images/Products/' +
                                  productCartEle.productImg)}
                                className="img-thumbnail"
                                alt="product"
                                height="48px"
                                width="48px"
                              />
                            </TableCell>
                            <TableCell align="left">{productCartEle.productName}</TableCell>
                            <TableCell align="center">
                              {productCartEle.productPrice.toLocaleString('vi-VN', {
                                style: 'currency',
                                currency: 'VND',
                              })}
                            </TableCell>
                            <TableCell align="center">
                              <input
                                type="number"
                                value={productCartEle.productQuantity}
                                className="form-control"
                                style={{ width: '75px' }}
                                onChange={(event) =>
                                  onProductCartQuantityChange(event.target.value, productCartEle)
                                }
                              />
                            </TableCell>
                            <TableCell align="center">
                              {(
                                productCartEle.productPrice * productCartEle.productQuantity
                              ).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}
                            </TableCell>
                          </TableRow>
                        )
                      })
                    ) : (
                      <TableRow>
                        <TableCell>No product...</TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </TableContainer>
            </Paper>
            <Stack direction="row" spacing={2} justifyContent="space-between" marginTop={3}>
              <Typography variant="h6" sx={{ fontWeight: 'bold' }}>
                TOTAL
              </Typography>
              <Typography variant="h6" sx={{ fontWeight: 'bold' }}>
                {total.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}
              </Typography>
            </Stack>
            <TextareaAutosize
              aria-label="note"
              minRows={2}
              placeholder="Note..."
              style={{ width: '100%' }}
              value={noteModal}
              onChange={(event) => {
                setNoteModal(event.target.value)
              }}
            />
            <Stack direction="row" spacing={2} justifyContent="space-between" marginTop={1}>
              <Button type="submit" variant="contained" color="success" startIcon={<PostAddIcon />}>
                Create
              </Button>
              <Button variant="outlined" onClick={handleCloseModalCreateOrder}>
                Cancel
              </Button>
            </Stack>
          </form>
        </Box>
      </Modal>

      {/* Modal Update Order */}
      <Modal open={openModalUpdateOrder} onClose={handleCloseModalUpdateOrder}>
        <Box sx={style} style={{ width: '90%', maxHeight: '95%', overflow: 'auto' }}>
          <h4 className="text-center">Update Order</h4>
          <form onSubmit={onSubmitUpdateModalHandle}>
            <Grid container spacing={1}>
              <Grid item xs={12}>
                <Stack component="div" direction="row" spacing={2}>
                  <TextareaAutosize
                    aria-label="note"
                    minRows={2}
                    placeholder="Note..."
                    style={{ width: '100%' }}
                    value={noteModal}
                    onChange={(event) => {
                      setNoteModal(event.target.value)
                    }}
                  />
                  <TextField
                    label="Order Date"
                    type="date"
                    variant="standard"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    value={orderDateModal}
                    onChange={(event) => setOrderDateModal(event.target.value)}
                    disabled
                  />
                  <TextField
                    label="Shipped Date"
                    type="date"
                    variant="standard"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    value={shippedDateModal}
                    onChange={(event) => setShippedDateModal(event.target.value)}
                  />
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <TableContainer component={Paper}>
                  <Table aria-label="cart table">
                    <TableHead>
                      <TableRow className="bg-secondary bg-gradient">
                        <TableCell align="center" colSpan={3} sx={{ color: 'white' }}>
                          PRODUCT
                        </TableCell>
                        <TableCell align="center" sx={{ color: 'white' }}>
                          PRICE
                        </TableCell>
                        <TableCell align="center" sx={{ color: 'white' }}>
                          QUANTITY
                        </TableCell>
                        <TableCell align="center" sx={{ color: 'white' }}>
                          SUBTOTAL
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell align="left">
                          <IconButton onClick={onBtnAddProductToCartClick}>
                            <AddShoppingCartIcon fontSize="small" />
                          </IconButton>
                        </TableCell>
                        <TableCell align="right">
                          <img
                            src={require('../../assets/images/Products/' + productImageAddCart)}
                            className="img-thumbnail"
                            alt="product"
                            height="48px"
                            width="48px"
                          />
                        </TableCell>
                        <TableCell align="left">
                          <FormControl sx={{ minWidth: 100 }} size="small">
                            <Select
                              displayEmpty
                              inputProps={{ 'aria-label': 'Without label' }}
                              fullWidth
                              value={productEleAddCart}
                              onChange={(event) => onSelectProductAddCartChange(event.target.value)}
                            >
                              {productsData.length > 0 ? (
                                productsData.map((ele, index) => {
                                  return (
                                    <MenuItem key={index} value={ele}>
                                      {ele.name}
                                    </MenuItem>
                                  )
                                })
                              ) : (
                                <MenuItem></MenuItem>
                              )}
                            </Select>
                          </FormControl>
                        </TableCell>
                        <TableCell align="center">
                          {productPriceAddCart.toLocaleString('vi-VN', {
                            style: 'currency',
                            currency: 'VND',
                          })}
                        </TableCell>
                        <TableCell align="center">
                          <input
                            type="number"
                            value={productQuantityAddCart}
                            className="form-control"
                            style={{ width: '75px' }}
                            onChange={(event) => onInpQuantityAddCartChange(event.target.value)}
                          />
                        </TableCell>
                        <TableCell align="center">
                          {(productPriceAddCart * productQuantityAddCart).toLocaleString('vi-VN', {
                            style: 'currency',
                            currency: 'VND',
                          })}
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                </TableContainer>
                <h6 className="pt-3">Your Cart:</h6>
                <Paper style={{ maxHeight: 180, overflow: 'auto' }}>
                  <TableContainer component={Paper}>
                    <Table aria-label="cart table">
                      <TableBody>
                        {productCartsArr.length > 0 ? (
                          productCartsArr.map((productCartEle, index) => {
                            return (
                              <TableRow key={index}>
                                <TableCell align="left">
                                  <IconButton
                                    onClick={() => onClickDeleteProductCartHandle(productCartEle)}
                                  >
                                    <DeleteIcon fontSize="small" />
                                  </IconButton>
                                </TableCell>
                                <TableCell align="right">
                                  <img
                                    src={require('../../assets/images/Products/' +
                                      productCartEle.productImg)}
                                    className="img-thumbnail"
                                    alt="product"
                                    height="48px"
                                    width="48px"
                                  />
                                </TableCell>
                                <TableCell align="left">{productCartEle.productName}</TableCell>
                                <TableCell align="center">
                                  {productCartEle.productPrice.toLocaleString('vi-VN', {
                                    style: 'currency',
                                    currency: 'VND',
                                  })}
                                </TableCell>
                                <TableCell align="center">
                                  <input
                                    type="number"
                                    value={productCartEle.productQuantity}
                                    className="form-control"
                                    style={{ width: '75px' }}
                                    onChange={(event) =>
                                      onProductCartQuantityChange(
                                        event.target.value,
                                        productCartEle,
                                      )
                                    }
                                  />
                                </TableCell>
                                <TableCell align="center">
                                  {(
                                    productCartEle.productPrice * productCartEle.productQuantity
                                  ).toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}
                                </TableCell>
                              </TableRow>
                            )
                          })
                        ) : (
                          <TableRow>
                            <TableCell>No product...</TableCell>
                          </TableRow>
                        )}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </Paper>
                <Stack direction="row" spacing={2} justifyContent="space-between" marginTop={3}>
                  <Typography variant="h6" sx={{ fontWeight: 'bold' }}>
                    TOTAL
                  </Typography>
                  <Typography variant="h6" sx={{ fontWeight: 'bold' }}>
                    {total.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}
                  </Typography>
                </Stack>
              </Grid>
            </Grid>
            <Stack direction="row" spacing={2} justifyContent="space-between" marginTop={1}>
              <Button type="submit" variant="contained" color="primary" startIcon={<UploadIcon />}>
                Update
              </Button>
              <Button variant="outlined" onClick={handleCloseModalUpdateOrder}>
                Cancel
              </Button>
            </Stack>
          </form>
        </Box>
      </Modal>

      {/* Modal delete order */}
      <Modal open={openModalDeleteOrder} onClose={handleCloseModalDeleteOrder}>
        <Box sx={style} style={{ width: '50%' }}>
          <Typography variant="h4" component="h2" textAlign="center" marginBottom={2}>
            Confirm xóa đơn hàng!
          </Typography>
          <Divider />
          <form onSubmit={onSubmitDeleteModal}>
            <Grid container spacing={1} marginTop={3} marginBottom={3}>
              <Grid item xs={12}>
                <Typography variant="h6">Bạn có chắc chắn muốn xóa đơn hàng này không?</Typography>
              </Grid>
            </Grid>
            <Divider />
            <Grid container spacing={2} marginTop={3} justifyContent="space-around">
              <Button type="submit" variant="contained" color="error" startIcon={<DeleteIcon />}>
                Confirm
              </Button>
              <Button variant="outlined" onClick={handleCloseModalDeleteOrder}>
                Cancel
              </Button>
            </Grid>
          </form>
        </Box>
      </Modal>
    </div>
  )
}

export default CustomerOrderDetail
