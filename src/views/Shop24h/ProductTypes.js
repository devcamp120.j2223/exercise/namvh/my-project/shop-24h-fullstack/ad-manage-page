import React, { useEffect, useState } from 'react'
import { useSnackbar } from 'notistack'
import { CCard, CCardBody, CCardHeader } from '@coreui/react'
import {
  Box,
  Button,
  Grid,
  IconButton,
  InputBase,
  Modal,
  Pagination,
  Paper,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextareaAutosize,
  TextField,
} from '@mui/material'
import LocalOfferIcon from '@mui/icons-material/LocalOffer'
import EditIcon from '@mui/icons-material/Edit'
import UploadIcon from '@mui/icons-material/Upload'
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline'
import ManageSearchIcon from '@mui/icons-material/ManageSearch'

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '80%',
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 2,
}

const ProductTypes = () => {
  const { enqueueSnackbar } = useSnackbar()
  const [productTypesData, setProductTypesData] = useState([
    {
      description: '',
      name: '',
    },
  ])
  const [typeNameModal, setTypeNameModal] = useState('')
  const [descriptionModal, setDescriptionModal] = useState('')
  const [rowId, setRowId] = useState('')

  const fetchApi = async (url, body) => {
    const response = await fetch(url, body)
    const data = await response.json()
    return data
  }

  //set page pagination
  const [noPage, setNoPage] = useState(0)
  const [page, setPage] = useState(1)
  const onPageChange = (event, value) => {
    setPage(value)
  }

  //biến dùng để mount lại page
  const [refresh, setRefresh] = useState(false)
  const getRefresh = () => {
    refresh === true ? setRefresh(false) : setRefresh(true)
  }
  //Search
  const [inputTypeSearch, setInputTypeSearch] = useState('')
  //khai báo url
  const URL = 'http://localhost:8000/ProductTypes?name=' + inputTypeSearch

  //set Modal
  const [createStatus, setCreateStatus] = useState(true)
  const [openModal, setOpenModal] = useState(false)
  const handleOpenModal = () => setOpenModal(true)
  const handleCloseModal = () => {
    setOpenModal(false)
    resetModal()
  }
  const resetModal = () => {
    setTypeNameModal('')
    setDescriptionModal('')
  }

  //sự kiện click button create
  const onClickCreateButtonHandle = () => {
    setCreateStatus(true)
    handleOpenModal()
  }
  //sự kiện click button edit
  const onClickEditBtnHandle = (rowData) => {
    setCreateStatus(false)
    setRowId(rowData._id)
    setTypeNameModal(rowData.name)
    setDescriptionModal(rowData.description)
    handleOpenModal()
  }
  //sự kiện submit form
  const onSubmitModalHandle = () => {
    let productTypeData = {
      name: typeNameModal.trim(),
      description: descriptionModal ? descriptionModal.trim() : '',
    }
    if (createStatus) {
      createProductType(productTypeData)
    } else {
      updateProductType(productTypeData)
    }
  }

  //hàm tạo product type mới
  const createProductType = (productTypeData) => {
    let body = {
      method: 'POST',
      body: JSON.stringify(productTypeData),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    fetchApi('http://localhost:8000/ProductTypes', body)
      .then((response) => {
        if (response.status === 'Internal server error') {
          if (response.messager.includes('name_1 dup key')) {
            enqueueSnackbar('tên loại sản phẩm đã có', { variant: 'error' })
            throw new Error('tên loại sản phẩm đã có')
          } else throw new Error('something wrong')
        } else {
          enqueueSnackbar('đã tạo loại sản phẩm mới', { variant: 'success' })
          //đóng modal
          handleCloseModal()
          //refresh table
          getRefresh()
        }
      })
      .catch((error) => {
        throw error
      })
  }

  //hàm cập nhật product type
  const updateProductType = (productTypeData) => {
    let body = {
      method: 'PUT',
      body: JSON.stringify(productTypeData),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
    fetchApi('http://localhost:8000/ProductTypes/' + rowId, body)
      .then((response) => {
        if (response.status === 'Internal server error') {
          if (response.message.includes('name_1 dup key')) {
            enqueueSnackbar('tên loại sản phẩm đã có', { variant: 'error' })
            throw new Error('tên loại sản phẩm đã có')
          } else throw new Error('something wrong')
        } else {
          enqueueSnackbar('đã cập nhật loại sản phẩm', { variant: 'success' })
          //đóng modal
          handleCloseModal()
          //refresh table
          getRefresh()
        }
      })
      .catch((error) => {
        throw error
      })
  }

  useEffect(() => {
    fetchApi(URL)
      .then((typesData) => {
        setProductTypesData(typesData.data)
        setNoPage(Math.ceil(typesData.data.length / 5))
      })
      .catch((error) => {
        throw error
      })
  }, [refresh, URL])

  return (
    <div>
      <CCard className="mb-4">
        <CCardHeader>
          <h3>Product Types Table</h3>
        </CCardHeader>
        <CCardBody>
          <Stack spacing={2}>
            <div>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  <Button
                    variant="contained"
                    color="success"
                    endIcon={<LocalOfferIcon />}
                    size="large"
                    className="float-end"
                    onClick={onClickCreateButtonHandle}
                  >
                    Create
                  </Button>
                </Grid>
                <Grid item>
                  <Paper
                    component="form"
                    sx={{ display: 'flex', alignItems: 'center', paddingX: 2 }}
                  >
                    <InputBase
                      placeholder="Search Type Name....."
                      value={inputTypeSearch}
                      onChange={(event) => setInputTypeSearch(event.target.value)}
                    />
                    <IconButton type="submit" sx={{ p: '10px' }} aria-label="search" disabled>
                      <ManageSearchIcon />
                    </IconButton>
                  </Paper>
                </Grid>
              </Grid>
            </div>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="cart table">
                <TableHead>
                  <TableRow className="bg-secondary bg-gradient">
                    <TableCell align="center">#</TableCell>
                    <TableCell align="center">Name</TableCell>
                    <TableCell align="center">Description</TableCell>
                    <TableCell align="center">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {productTypesData.slice(5 * (page - 1), 5 * page).map((ele, index) => {
                    return (
                      <TableRow key={index}>
                        <TableCell align="center">{index + 1}</TableCell>
                        <TableCell align="center">{ele.name}</TableCell>
                        <TableCell>{ele.description}</TableCell>
                        <TableCell align="center">
                          <Button
                            variant="contained"
                            color="info"
                            startIcon={<EditIcon />}
                            size="small"
                            onClick={() => onClickEditBtnHandle(ele)}
                          >
                            Edit
                          </Button>
                        </TableCell>
                      </TableRow>
                    )
                  })}
                </TableBody>
              </Table>
            </TableContainer>
            <Grid sx={{ display: 'flex', justifyContent: 'flex-end' }}>
              <Pagination
                showFirstButton
                showLastButton
                count={noPage}
                page={page}
                onChange={onPageChange}
              />
            </Grid>
          </Stack>
        </CCardBody>
      </CCard>

      {/* Modal */}
      <Modal open={openModal} onClose={handleCloseModal}>
        <Box sx={style} style={{ maxHeight: '80%', overflow: 'auto' }}>
          <h4 className="text-center">
            {createStatus ? 'CREATE PRODUCT TYPES' : 'EDIT PRODUCT TYPES'}
          </h4>
          <form onSubmit={onSubmitModalHandle}>
            <Stack spacing={3}>
              <Stack spacing={1}>
                <label>Type Name</label>
                <TextField
                  required
                  variant="outlined"
                  value={typeNameModal}
                  fullWidth
                  onChange={(event) => setTypeNameModal(event.target.value)}
                />
              </Stack>
              <Stack spacing={1}>
                <label>Description</label>
                <TextareaAutosize
                  aria-label="Description"
                  minRows={5}
                  placeholder="Description..."
                  style={{ width: '100%' }}
                  value={descriptionModal}
                  onChange={(event) => {
                    setDescriptionModal(event.target.value)
                  }}
                />
              </Stack>
            </Stack>
            <Stack direction="row" spacing={2} justifyContent="space-between" marginTop={1}>
              {createStatus ? (
                <Button
                  type="submit"
                  variant="contained"
                  color="success"
                  startIcon={<AddCircleOutlineIcon />}
                >
                  Create
                </Button>
              ) : (
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  startIcon={<UploadIcon />}
                >
                  Update
                </Button>
              )}
              <Button variant="outlined" onClick={handleCloseModal}>
                Cancel
              </Button>
            </Stack>
          </form>
        </Box>
      </Modal>
    </div>
  )
}

export default ProductTypes
